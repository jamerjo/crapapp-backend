package be.pxl.crapapp.model.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import be.pxl.crapapp.exception.ShipSizeRuntimeException;

/*
 * Testing abstract class Ship with DestroyerShip subclass.
 * 
 * DestroyerShip is the smallest of the ships, easing most tests.
 *
 */
public class ShipTests {
	private Ship ship;
	private List<GameBoardSquare> squares;
	
	@Before
	public void init() {
		squares = new ArrayList<>();
		squares.add(new GameBoardSquare('A', 1));
		squares.add(new GameBoardSquare('B', 1));
		ship = new DestroyerShip(squares);
	}
	
	@Test(expected=ShipSizeRuntimeException.class)
	public void ConstructorSizeTooSmallTest() {
		List<GameBoardSquare> squares = new ArrayList<>();
		squares.add(new GameBoardSquare('A', 1));
		Ship ship = new DestroyerShip(squares);
		assertNull(ship);
	}
	
	@Test(expected=ShipSizeRuntimeException.class)
	public void ConstructorSizeTooLargeTest() {
		List<GameBoardSquare> squares = new ArrayList<>();
		squares.add(new GameBoardSquare('A', 1));
		squares.add(new GameBoardSquare('B', 1));
		squares.add(new GameBoardSquare('C', 1));
		Ship ship = new DestroyerShip(squares);
		assertNull(ship);
	}
	
	@Test
	public void getActiveTest() {
		assertEquals(squares, ship.getActive());
	}
	
	@Test
	public void getIdTest() {
		assertEquals(0, ship.getId());
	}
	
	@Test
	public void isSunkTest() {
		assertFalse(ship.isSunk());
	}
	
	@Test
	public void hitTest() {
		assertTrue(ship.hit(squares.get(0)));
	}
	
	@Test
	public void hitMissTest() {
		assertFalse(ship.hit(new GameBoardSquare('J', 10)));
	}
	
	@Test
	public void sinkTest() {
		assertFalse(ship.isSunk());
		ship.hit(new GameBoardSquare('A',1));
		ship.hit(new GameBoardSquare('B',1));
		assertTrue(ship.isSunk());
	}
}
