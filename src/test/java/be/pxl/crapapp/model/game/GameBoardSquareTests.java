package be.pxl.crapapp.model.game;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

public class GameBoardSquareTests {
	private GameBoardSquare square;
	
	@Before
	public void init() {
		 square = new GameBoardSquare('A',1);
	}
	
	@Test
	public void getterTest() {
		assertEquals("getColumn error", 'A', square.getColumn());
		assertEquals("getRow error", 1 , square.getRow());
		assertEquals("getId error", 0,square.getId());
	}
	
	@Test
	public void equalsTest() {
		assertFalse("null error", square.equals(null));
		assertFalse("class error", square.equals(new Object()));
		assertFalse("row error", square.equals(new GameBoardSquare('A', 2)));
	}
	
	@Test
	public void hashCodeTest() {
		GameBoardSquare square2 = new GameBoardSquare('A',1);
		assertEquals(square.hashCode(), square2.hashCode());
	}
	
	@Test
	public void toStringTest() {
		assertEquals("A1", square.toString());
	}
}
