package be.pxl.crapapp.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.security.InvalidParameterException;

import org.junit.Test;

public class BugTests {
	@Test
	public void emptyConstructorTest() {
		Bug bug = new Bug();
		assertNotNull(bug);
	}
	
	@Test
	public void constructorTest() {
		String code = "BBCTAT";
		boolean active = false;
		
		Bug bug = new Bug(code, active);
		
		assertNotNull(bug);
		assertEquals(code, bug.getCode());
		assertEquals(active, bug.isActive());
	}
	
	@Test(expected = InvalidParameterException.class)
	public void constructorCodeLengthErrorTest() {
		String code = "BBCTATTOOLONG";
		boolean active = false;
		
		new Bug(code, active);
	}
	
	@Test
	public void setActiveTest() {
		String code = "BBSAAT";
		boolean active = false;
		
		Bug bug = new Bug(code, active);
		assertEquals(active, bug.isActive());
		
		boolean newActive = true;
		bug.setActive(newActive);
		assertEquals(newActive, bug.isActive());
	}
	
	@Test
	public void hashCodeTest() {
		Bug bug = new Bug("value",false);
		Bug bug2 = new Bug("value",false);
		
		assertEquals(bug.hashCode(), bug2.hashCode());
	}
	
	@Test
	public void equalsTest() {
		Bug bug = new Bug("value",false);
		Bug bug2 = new Bug("value",false);
		
		assertTrue(bug.equals(bug2));
	}
}
