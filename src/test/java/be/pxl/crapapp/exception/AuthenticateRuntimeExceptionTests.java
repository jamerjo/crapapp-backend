package be.pxl.crapapp.exception;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

public class AuthenticateRuntimeExceptionTests {

	@Test
	public void EmptyConstructorTest() {
		AuthenticateRuntimeException exception = new AuthenticateRuntimeException();
		assertNotNull(exception);
	}
	
	@Test
	public void NestedExceptionConstructorTest() {
		AuthenticateRuntimeException exception = new AuthenticateRuntimeException(new AuthenticateRuntimeException());
		assertNotNull(exception);
		assertNotNull(exception.getCause());
	}
}
