package be.pxl.crapapp.controller;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(value = BugController.class)
public class BugControllerTests {	
	@MockBean
	private BugService bugService;
	
	@MockBean
	private UserService userService;
	
	@MockBean
	private ProfilePictureService profilePictureService;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	private String URI = "/api/bug";

	@Test
	public void getAllBugsTest() throws Exception {
		List<Bug> bugs = new ArrayList<>();
		bugs.add(new Bug("BTGEBC", false));
		Mockito.when(bugService.getAllBugs()).thenReturn(bugs);

		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(200, response.getStatus());
		assertEquals(objectMapper.writeValueAsString(bugs), response.getContentAsString());
	}

	@Test
	public void getAllBugsNotFoundTest() throws Exception {
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(404, response.getStatus());
		Mockito.verify(bugService).getAllBugs();
	}

	@Test
	public void updateAllBugsTest() throws Exception {
		List<Bug> bugs = new ArrayList<>();
		Bug bug = new Bug("BTUBBC", false);
		bugs.add(bug);
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI).content(objectMapper.writeValueAsString(bugs))
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(200, response.getStatus());
		Mockito.verify(bugService).updateBug(bug);
	}

}
