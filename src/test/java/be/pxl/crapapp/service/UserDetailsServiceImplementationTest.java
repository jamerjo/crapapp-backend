package be.pxl.crapapp.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.repository.BugRepository;
import be.pxl.crapapp.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserDetailsServiceImplementationTest {
	@Mock
	private UserRepository repository;
	
	@Mock
	private BugRepository bugRepository;

	@InjectMocks
	private UserDetailsServiceImplementation service;

	private be.pxl.crapapp.model.User testUser;
	
	@Test
	public void loadUserByUsernameTest() {
		testUser = new be.pxl.crapapp.model.User("test@test.com", "test", "test", "test", "USER", new ProfilePicture());
		Mockito.when(repository.findById(testUser.getEmailAddress())).thenReturn(Optional.of(testUser));
		Mockito.when(bugRepository.findById("BB_LP_WP")).thenReturn(Optional.of(new Bug("BB_LP_WP", false)));
		ArrayList<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
		simpleGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		UserDetails expectUser = new User("test@test.com", "test", simpleGrantedAuthorities);
		
		UserDetails returnUser = service.loadUserByUsername("test@test.com");
		
		assertEquals(expectUser, returnUser);
	}
	
	@Test
	public void loadAdminByUsernameTest() {
		testUser = new be.pxl.crapapp.model.User("test@test.com", "test", "test", "test", "ADMIN", new ProfilePicture());
		Mockito.when(repository.findById(testUser.getEmailAddress())).thenReturn(Optional.of(testUser));
		Mockito.when(bugRepository.findById("BB_LP_WP")).thenReturn(Optional.of(new Bug("BB_LP_WP", false)));
		ArrayList<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<>();
		simpleGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
		simpleGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
		UserDetails expectUser = new User("test@test.com", "test", simpleGrantedAuthorities);
		
		UserDetails returnUser = service.loadUserByUsername("test@test.com");
		
		assertEquals(expectUser, returnUser);
	}
	
	@Test(expected= UsernameNotFoundException.class)
	public void loadNonExistingUserByUsernameTest() {
		service.loadUserByUsername("test@test.com");
	}

}
