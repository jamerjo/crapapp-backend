package be.pxl.crapapp.service;

import static org.mockito.ArgumentMatchers.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplementationTests {
	@Mock
	private JavaMailSender javaMailSender;
	
	@InjectMocks
	private EmailServiceImplementation service;
	
	@Test(expected = Test.None.class)
	public void sendConfirmationMailTest() {
		service.sendConfirmationEmail("test@test.com", "token", "ipAddress");
		Mockito.verify(javaMailSender).send(any(SimpleMailMessage.class));
	}
	
	@Test(expected = Test.None.class)
	public void sendForgotPasswordMailTest() {
		service.sendForgotPasswordEmail("test@test.com", "token", "ipAddress");
		Mockito.verify(javaMailSender).send(any(SimpleMailMessage.class));
	}
	
	@Test(expected= Test.None.class)
	public void sendForgotUsernameMailTest() {
		service.sendForgotUsernameEmail("test@test.com", "test");
		Mockito.verify(javaMailSender).send(any(SimpleMailMessage.class));
	}
	
	@Test(expected= Test.None.class)
	public void sendBlacklistMailTest() {
		service.sendBlacklistedEmail("test@test.com", "admin@admin.admin");
		Mockito.verify(javaMailSender).send(any(SimpleMailMessage.class));
	}
	
	@Test(expected= Test.None.class)
	public void sendUnBlacklistMailTest() {
		service.sendUnBlacklistedEmail("test@test.com");
		Mockito.verify(javaMailSender).send(any(SimpleMailMessage.class));
	}
}
