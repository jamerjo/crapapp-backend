package be.pxl.crapapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import be.pxl.crapapp.model.game.Game;

public interface GameRepository extends JpaRepository<Game, Long> {

}
