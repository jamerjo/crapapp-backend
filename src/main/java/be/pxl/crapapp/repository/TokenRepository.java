package be.pxl.crapapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import be.pxl.crapapp.model.Token;

@Repository
public interface TokenRepository extends JpaRepository<Token, String> {

}
