package be.pxl.crapapp.exception;

public class ShipSizeRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	public ShipSizeRuntimeException() {
		super();
	}
	
	public ShipSizeRuntimeException(String message) {
		super(message);
	}
	
	public ShipSizeRuntimeException(Exception e) {
		super(e);
	}
	
	public ShipSizeRuntimeException(String message, Exception e) {
		super(message, e);
	}

}
