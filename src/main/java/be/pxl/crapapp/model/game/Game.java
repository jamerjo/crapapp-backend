package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import be.pxl.crapapp.exception.WrongPlayerException;
import be.pxl.crapapp.model.User;

@Entity
public class Game {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private long id;
	
	@OneToOne
	@Cascade(CascadeType.PERSIST)
	private User player1;
	@OneToOne
	@Cascade(CascadeType.PERSIST)
	private GameBoard board1;
	@OneToOne
	@Cascade(CascadeType.PERSIST)
	private User player2;
	@OneToOne
	@Cascade(CascadeType.PERSIST)
	private GameBoard board2;
	private boolean singleplayer;
	
	public Game() {
		board1 = new GameBoard();
		board2 = new GameBoard();
	}
	
	public Game(User player1, User player2, boolean singleplayer) {
		this();
		this.player1 = player1;
		this.player2 = player2;
		this.singleplayer = singleplayer;
	}
	
	public long getId() {
		return id;
	}

	public User getPlayer1() {
		return player1;
	}

	public GameBoard getBoard1() {
		return board1;
	}

	public User getPlayer2() {
		return player2;
	}

	public GameBoard getBoard2() {
		return board2;
	}

	public boolean isSingleplayer() {
		return singleplayer;
	}

	public boolean move(User currentPlayer, GameBoardSquare square) {
		GameBoard aimedAtBoard = null;
		if(currentPlayer.equals(player1)) {
			aimedAtBoard = board2;
		} else if(currentPlayer.equals(player2)) {
			aimedAtBoard = board1;
		} else {
			throw new WrongPlayerException();
		}
		if(aimedAtBoard.fire(square)) {
			currentPlayer.setHighscore(currentPlayer.getHighscore() + 50);
			return true;
		} else {
			currentPlayer.setHighscore(currentPlayer.getHighscore() - 15);
			return false;
		}
	}
	
	public void placeShips(User currentPlayer, List<Ship> ships) {
		GameBoard playerBoard = null;
		if(currentPlayer.equals(player1)) {
			playerBoard = board1;
		} else if(currentPlayer.equals(player2)) {
			playerBoard = board2;
		} else {
			throw new WrongPlayerException();
		}
		playerBoard.placeShips(ships);
	}
	
	public User checkWinner() {
		if(board1.allSunk()) {
			return player2;
		} else if(board2.allSunk()) {
			return player1;
		} else {
			return null;
		}
	}
}
