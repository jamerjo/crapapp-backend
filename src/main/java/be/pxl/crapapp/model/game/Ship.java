package be.pxl.crapapp.model.game;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import be.pxl.crapapp.exception.ShipSizeRuntimeException;

@JsonTypeInfo(
		  use = JsonTypeInfo.Id.NAME, 
		  include = JsonTypeInfo.As.PROPERTY, 
		  property = "type")
		@JsonSubTypes({ 
		  @Type(value = BattleShip.class, name = "battleship"), 
		  @Type(value = CarrierShip.class, name = "carrier"),
		  @Type(value = CruiserShip.class, name= "cruiser"),
		  @Type(value = DestroyerShip.class, name= "destroyer"),
		  @Type(value = SubmarineShip.class, name= "submarine")
		})
@Entity
public abstract class Ship {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@OneToMany
	@Cascade(CascadeType.PERSIST)
	private List<GameBoardSquare> active;
	private boolean sunk;

	@Transient
	private short size;
	
	public Ship() {}

	public Ship(List<GameBoardSquare> location, short size) {
		this.size = size;
		if (location.size() < size) {
			throw new ShipSizeRuntimeException("Ship too small!");
		}
		if (location.size() > size) {
			throw new ShipSizeRuntimeException("Ship too big!");
		}
		this.active = location;
	}

	public long getId() {
		return id;
	}

	public List<GameBoardSquare> getActive() {
		return active;
	}

	public boolean isSunk() {
		return sunk;
	}

	public boolean hit(GameBoardSquare square) {
		if (!active.contains(square)) {
			return false;
		}
		active.remove(square);
		if (active.isEmpty()) {
			sunk = true;
		}
		return true;
	}
	
	@Override
	public String toString() {
		StringBuilder string = new StringBuilder(this.getClass().getSimpleName());
		string.append(": ");
		for(GameBoardSquare square: active) {
			string.append(square.toString());
			string.append(", ");
		}
		return string.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + (sunk ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ship other = (Ship) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (sunk != other.sunk)
			return false;
		return true;
	}
}
