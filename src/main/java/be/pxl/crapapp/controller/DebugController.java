package be.pxl.crapapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.RegisterCatch;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@RequestMapping("/api/debug")
public class DebugController {
	@Autowired
	private UserService userService;

	@Autowired
	private ProfilePictureService profilePictureService;

	@PostMapping(value = "/register", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Registers confirmed user.", notes = "User registers with data, creates new confirmed user. Purely for debug!", response = User.class)
	@ApiResponses({ @ApiResponse(code = 201, message = "Created", response = User.class) })
	public ResponseEntity<Object> register(@RequestBody RegisterCatch registerCatch) {
		User newUser = new User(registerCatch.getEmail(), registerCatch.getUsername(), registerCatch.getPassword(),
				registerCatch.getGender(), "USER", profilePictureService.getProfilePictureById(1));
		newUser.setConfirmed(true);
		userService.addUser(newUser);
		return new ResponseEntity<>(newUser, HttpStatus.CREATED);
	}

	@GetMapping(value = "/user/{username}", produces = "application/json")
	@ApiOperation(value = "Returns user.", notes = "Returns a user based on given username (not ID). Purely for debug!", response = User.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "OK", response = User.class),
			@ApiResponse(code = 404, message = "Not found!") })
	public ResponseEntity<Object> getUser(@ApiParam(required = true) @PathVariable("username") String username) {
		User user = userService.getByUsername(username);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
