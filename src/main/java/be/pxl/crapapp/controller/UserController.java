package be.pxl.crapapp.controller;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.Token;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.EmailService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.TokenService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.EmailCatch;
import be.pxl.crapapp.support.ErrorResponse;
import be.pxl.crapapp.support.LoginCatch;
import be.pxl.crapapp.support.PasswordConfirmationCatch;
import be.pxl.crapapp.support.RegisterCatch;
import be.pxl.crapapp.support.UpdateCatch;
import be.pxl.crapapp.support.ValidationError;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping("/api/user")
@Api("/api/user")
public class UserController {
	@Autowired
	private BCryptPasswordEncoder encoder;

	@Autowired
	private ProfilePictureService profilePictureService;

	@Autowired
	private UserService userService;

	@Autowired
	private TokenService tokenService;

	@Autowired
	private EmailService mailSender;

	@Autowired
	private BugService bugService;

	private Logger logger = LoggerFactory.getLogger(UserController.class);

	@PostMapping("/login")
	@ApiOperation(value = "Login", notes = "Logs in a user", response = String.class)
	@ApiResponses({ @ApiResponse(code = 202, message = "Succes", response = Boolean.class),
			@ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 401, message = "Unauthorized"),
			@ApiResponse(code = 403, message = "Forbidden: Check email") })
	public ResponseEntity<Object> login(@RequestBody LoginCatch loginCatch) {
		String username = loginCatch.getUsername();
		String password = loginCatch.getPassword();

		User user = userService.getByUsername(username);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		if (!user.isEnabled() || !user.isConfirmed()) {
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}

		boolean value = user.checkPassword(password);
		if (bugService.getBugByCode("BB_LP_WP").isActive()) {
			value = true;
		}

		if (value) {
			String id = user.getEmailAddress();
			return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>(value, HttpStatus.UNAUTHORIZED);
		}
	}

	@PostMapping(value = "/register", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Register", notes = "User registers with data, creates new user", response = User.class)
	@ApiResponses({ @ApiResponse(code = 201, message = "Created", response = User.class),
			@ApiResponse(code = 400, message = "Bad Request", response = ErrorResponse.class),
			@ApiResponse(code = 409, message = "Username is already in use."),
			@ApiResponse(code = 412, message = "Emailaddress already in use.") })
	public ResponseEntity<Object> register(@RequestBody RegisterCatch registerCatch, HttpServletRequest request) {
		String id = registerCatch.getEmail();
		String username = registerCatch.getUsername();
		String password = registerCatch.getPassword();
		String gender = registerCatch.getGender();
		String confirmPassword = registerCatch.getConfirmPassword();

		if (!password.equals(confirmPassword)) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Confirmation password is incorrect.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("confirmationPassword");
			valError.setReason("Does not match password.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		String regexMail = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$";

		Pattern patternMail = Pattern.compile(regexMail);

		Matcher matcherMail = patternMail.matcher(id);

		if (!matcherMail.find()) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Mail is not in the correct format");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("email");
			valError.setReason("Incorrect format.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		if (username.length() < 3 || username.length() > 20) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Username does not have a valid length.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("username");
			valError.setReason("Incorrect length.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		if (!usernameValidation(username)) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Username may only contain letters and numbers.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("username");
			valError.setReason("Username may only contain letters and numbers.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		if (userService.getByUsername(username) != null) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		if (userService.getById(id) != null) {
			return new ResponseEntity<>(HttpStatus.PRECONDITION_FAILED);
		}

		User newUser = new User(id, username, password, gender, "USER", profilePictureService.getProfilePictureById(1));

		userService.addUser(newUser);

		Token token = Token.generateToken(newUser);

		tokenService.addToken(token);

		mailSender.sendConfirmationEmail(newUser.getEmailAddress(), token.getToken(), getIp(request));

		return new ResponseEntity<>(newUser, HttpStatus.CREATED);
	}

	@GetMapping(value = "/validate/{token}")
	@ApiOperation(value = "Validation", notes = "Token validation")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK!", response = String.class),
			@ApiResponse(code = 404, message = "Token is invalid.") })
	public ResponseEntity<Object> validate(
			@ApiParam(name = "token", required = true) @PathVariable("token") String token) {
		Token dbToken = tokenService.getTokenById(token);
		if (dbToken == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		User user = dbToken.getUser();
		user.setConfirmed(true);
		userService.updateUser(user);
		tokenService.deleteToken(dbToken);

		return new ResponseEntity<>(user.getUsername(), HttpStatus.OK);
	}

	@GetMapping(value = "/validate/password/{token}")
	@ApiOperation(value = "Validation", notes = "Token validation")
	@ApiResponses({ @ApiResponse(code = 200, message = "OK!"),
			@ApiResponse(code = 404, message = "Token is invalid.") })
	public ResponseEntity<Object> validatePasswordToken(
			@ApiParam(name = "token", required = true) @PathVariable("token") String token) {
		Token dbToken = tokenService.getTokenById(token);
		if (dbToken == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PutMapping(value = "/update", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Update", notes = "Update user profile", response = User.class, authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 202, message = "Accepted", response = User.class),
			@ApiResponse(code = 400, message = "Bad Request", response = ErrorResponse.class),
			@ApiResponse(code = 409, message = "Username is already in use."),
			@ApiResponse(code = 403, message = "Unauthorized") })
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Object> update(@RequestBody UpdateCatch updateUser) {
		User oldUser = retrieveUserFromAuthToken();

		String username = updateUser.getUsername();
		String password = updateUser.getPassword();
		String oldPassword = updateUser.getOldPassword();
		String gender = updateUser.getGender();

		if (userService.getByUsername(username) != null && !userService.getByUsername(username).equals(oldUser)) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}

		oldUser.setUsername(username);
		oldUser.setGender(gender);

		if (oldPassword.equals("a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26")) {
			userService.updateUser(oldUser);
			return new ResponseEntity<>(oldUser, HttpStatus.ACCEPTED);
		} else if (oldUser.checkPassword(oldPassword)) {
			if (oldPassword.equals(password)) {
				ErrorResponse error = new ErrorResponse();
				error.setMessage("New password may not be the same as old password.");
				ValidationError[] validationFailures = new ValidationError[1];
				ValidationError valError = new ValidationError();
				valError.setField("password");
				valError.setReason("New password may not be the same as old password.");
				error.setValidationFailures(validationFailures);
				return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
			}
			oldUser.setPassword(password);
			userService.updateUser(oldUser);
			return new ResponseEntity<>(oldUser, HttpStatus.ACCEPTED);
		} else {
			return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
		}
	}

	@GetMapping(value = "/profile", produces = "application/json")
	@ApiOperation(value = "Retrieve User", notes = "Retrieving user data", response = User.class, authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "OK", response = User.class),
			@ApiResponse(code = 404, message = "Not Found") })
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<User> getUser() {
		User user = retrieveUserFromAuthToken();
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<>(user, HttpStatus.OK);
	}

	@DeleteMapping(value = "/delete", produces = "application/json")
	@ApiOperation(value = "Delete user", notes = "(Soft) delete of user profile.", authorizations = {
			@Authorization(value = "apikey") })
	@ApiResponses({ @ApiResponse(code = 200, message = "OK"), @ApiResponse(code = 404, message = "Not Found") })
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<User> deleteUser() {
		User user = retrieveUserFromAuthToken();

		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		user.setEnabled(false);
		userService.updateUser(user);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/forgotpassword", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "Forgot password", notes = "Initiate request for new password.")
	@ApiResponses({ @ApiResponse(code = 404, message = "Not Found"), @ApiResponse(code = 200, message = "Ok!") })
	public ResponseEntity<HttpStatus> passwordForgotMail(@RequestBody EmailCatch email, HttpServletRequest request) {
		User user = userService.getById(email.getEmail());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		user.setConfirmed(false);
		userService.updateUser(user);
		Token token = Token.generateToken(user);
		tokenService.addToken(token);

		mailSender.sendForgotPasswordEmail(user.getEmailAddress(), token.getToken(), getIp(request));

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/forgotpassword/{token}", produces = "application/json", consumes = "application/json")
	@ApiOperation(value = "new password", notes = "Token validation and new password update", response = User.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "OK!"),
			@ApiResponse(code = 400, message = "Bad request", response = ErrorResponse.class),
			@ApiResponse(code = 404, message = "Token is invalid.") })
	public ResponseEntity<Object> validatePassword(
			@ApiParam(name = "token", required = true) @PathVariable("token") String token,
			@RequestBody PasswordConfirmationCatch passwordCatch) {
		Token dbToken = tokenService.getTokenById(token);
		if (dbToken == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}

		String password = passwordCatch.getPassword();
		String confirmPassword = passwordCatch.getConfirmPassword();
		if (!password.equals(confirmPassword)) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("Confirmation password is incorrect.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("confirmationPassword");
			valError.setReason("Does not match password.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		User user = dbToken.getUser();
		String oldPassword = user.getPassword();
		if (encoder.matches(password, oldPassword)) {
			ErrorResponse error = new ErrorResponse();
			error.setMessage("New password cannot be the same as old password.");
			ValidationError[] validationFailures = new ValidationError[1];
			ValidationError valError = new ValidationError();
			valError.setField("Password");
			valError.setReason("New password cannot be the same as old password.");
			error.setValidationFailures(validationFailures);
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		}

		user.setPassword(password);
		user.setConfirmed(true);
		userService.updateUser(user);
		tokenService.deleteToken(dbToken);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/forgotusername", consumes = "application/json")
	@ApiOperation(value = "Forgot username", notes = "User requests forgotten username.")
	@ApiResponses({ @ApiResponse(code = 404, message = "Not found"), @ApiResponse(code = 200, message = "Ok!") })
	public ResponseEntity<User> usernameForgotMail(@RequestBody EmailCatch email) {
		User user = userService.getById(email.getEmail());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		String username = user.getUsername();

		mailSender.sendForgotUsernameEmail(user.getEmailAddress(), username);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/requestvalidation", consumes = "application/json")
	@ApiOperation(value = "Request validation token", notes = "Request new validation token after forgotten password.")
	@ApiResponses({ @ApiResponse(code = 404, message = "Not found"), @ApiResponse(code = 200, message = "Ok!") })
	public ResponseEntity<Object> newValidationToken(@RequestBody EmailCatch email, HttpServletRequest request) {
		User user = userService.getById(email.getEmail());
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		Token token = Token.generateToken(user);
		tokenService.addToken(token);

		mailSender.sendConfirmationEmail(user.getEmailAddress(), token.getToken(), getIp(request));

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@PostMapping(value = "/uploadpicture")
	@ApiOperation(value = "Upload new profile picture", authorizations = { @Authorization(value = "apikey")})
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<Object> uploadPictures(@RequestBody MultipartFile filepond) {
		User user = retrieveUserFromAuthToken();
		try {
			byte[] bytes = filepond.getBytes();
			ProfilePicture pic = new ProfilePicture(bytes);
			profilePictureService.addProfilePicture(pic);
			user.setProfilePicture(pic);
			userService.updateUser(user);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value = "/downloadpicture/{username}")
	@ApiOperation(value = "Download profile picture")
	@ApiResponses({@ApiResponse(code = 200, message = "OK!")})
	public ResponseEntity<Object> getImage(@PathVariable String username, HttpServletResponse response,
			HttpServletRequest request) throws IOException {
		User user = userService.getByUsername(username);
		ProfilePicture profilePicture = user.getProfilePicture();

		response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
		response.getOutputStream().write(profilePicture.getImage());

		response.getOutputStream().close();
		return new ResponseEntity<>(HttpStatus.OK);
	}

	private User retrieveUserFromAuthToken() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String id = auth.getName();
		return userService.getById(id);
	}

	private boolean usernameValidation(String value) {
		String regexUsername = "^[A-Za-z0-9]+$";

		Pattern patternUsername = Pattern.compile(regexUsername);

		Matcher matcherUsername = patternUsername.matcher(value);

		return matcherUsername.find();
	}

	private String getIp(HttpServletRequest request) {
		try {
			return request.getRequestURL().substring(0, request.getRequestURL().indexOf(":8"));
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
}
