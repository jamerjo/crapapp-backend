package be.pxl.crapapp.support;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import be.pxl.crapapp.model.Bug;
import be.pxl.crapapp.model.ProfilePicture;
import be.pxl.crapapp.model.User;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.ProfilePictureService;
import be.pxl.crapapp.service.UserService;

@Component
public class DataLoader implements ApplicationRunner {
	private UserService userService;
	private ProfilePictureService profilePictureService;
	private BugService bugService;
	private Logger logger = LoggerFactory.getLogger(DataLoader.class);

	@Autowired
	public DataLoader(UserService userService, ProfilePictureService profilePictureService, BugService bugService) {
		this.userService = userService;
		this.profilePictureService = profilePictureService;
		this.bugService = bugService;
	}

	public void run(ApplicationArguments args) {
		try {
			seedImage("https://i.imgur.com/dUsntHG.png", "temp.png");
			seedImage("https://i.imgur.com/IaEgBBl.jpg", "temp2.jpg");
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		seedUsers();
		seedBugs();
	}

	private void seedBugs() {
		bugService.addBug(new Bug("FB_PP_EB", false)); // Front End Profile Page Edit Button - Edit profile button
														// doesn't work
		bugService.addBug(new Bug("FB_AP_UA", false)); // Front End Admin Page User Access - User can access admin pages
														// in front end
		bugService.addBug(new Bug("BB_LP_WP", false)); // Back End Login Page Wrong Password - User can login with the
														// wrong password
		bugService.addBug(new Bug("FB_LP_SM", false)); // Front End Login Page Spelling Mistake - Username misspelled on
														// login page
	}

	private void seedUsers() {
		String frontendKey = "7cbd8ba9ecb49c352c95605afbd1ac6807e236c3449b97e9871ff9b2b9886dc28e3a22e07500556a79cbeb731b9b98fa11fa703d3a0789a6b0784a95c64d8704";
		User user1 = new User("crapapp.testing@gmail.com", "admin", frontendKey, "Male", "ADMIN",
				profilePictureService.getProfilePictureById(1));
		user1.setConfirmed(true);
		user1.setHighscore(68658289);
		userService.addUser(user1);

		User user3 = new User("crapapp.testing2@gmail.com", "confirmeduser", frontendKey, "Male", "USER",
				profilePictureService.getProfilePictureById(1));
		user3.setConfirmed(true);
		user3.setHighscore(79827869);
		userService.addUser(user3);

		User user4 = new User("crapapp.testing3@gmail.com", "unconfirmeduser", frontendKey, "Male", "USER",
				profilePictureService.getProfilePictureById(1));
		user3.setConfirmed(false);
		user3.setHighscore(79827869);
		userService.addUser(user4);

		User ai = new User("ComputersCanPlayToo@NotReal.com", "Computer", "You'llNeverBeAbleToLoginWithThis",
				"Computer", "USER", profilePictureService.getProfilePictureById(2));
		userService.addUser(ai);
	}

	private void seedImage(String url, String tempFileName) throws IOException {
		try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());
				FileOutputStream fileOutputStream = new FileOutputStream(tempFileName);
				) {
			byte[] dataBuffer = new byte[1024];
			int bytesRead;
			while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
				fileOutputStream.write(dataBuffer, 0, bytesRead);
			}
			ProfilePicture defaultImage = new ProfilePicture(Files.readAllBytes(new File(tempFileName).toPath()));
			profilePictureService.addProfilePicture(defaultImage);
		} 		
	}
}