package be.pxl.crapapp.support;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.pxl.crapapp.model.game.BattleShip;
import be.pxl.crapapp.model.game.CarrierShip;
import be.pxl.crapapp.model.game.CruiserShip;
import be.pxl.crapapp.model.game.DestroyerShip;
import be.pxl.crapapp.model.game.GameBoard;
import be.pxl.crapapp.model.game.GameBoardSquare;
import be.pxl.crapapp.model.game.Ship;
import be.pxl.crapapp.model.game.SubmarineShip;

public class AiSupport {
	public static final SecureRandom RANDOM = new SecureRandom();
	
	public static GameBoardSquare randomSquare() {
		char randomColumn = (char) (RANDOM.nextInt(10) + 65);
		int randomRow = RANDOM.nextInt(10) + 1;
		return new GameBoardSquare(randomColumn, randomRow);
	}
	
	public static GameBoardSquare randomActiveSquare(GameBoard board) {
		char randomColumn = (char) (RANDOM.nextInt(10) + 65);
		int randomRow = RANDOM.nextInt(10) + 1;
		GameBoardSquare square = new GameBoardSquare(randomColumn, randomRow);
		while(!board.getActive().contains(square)) {
			randomColumn = (char) (RANDOM.nextInt(10) + 65);
			randomRow = RANDOM.nextInt(10) + 1;
			square = new GameBoardSquare(randomColumn, randomRow);
		}
		return square;
	}

	public static List<Ship> getShipPlacement() {
		List<Ship> ships = new ArrayList<>();
		List<GameBoardSquare> taken = new ArrayList<>();

		CarrierShip carrier = generateCarrierPlacement(taken);

		BattleShip battleship = null;
		while (battleship == null) {
			battleship = generateBattleshipPlacement(taken);
		}
		
		CruiserShip cruiser = null;
		while (cruiser == null) {
			cruiser = generateCruiserPlacement(taken);
		}
		
		SubmarineShip submarine = null;
		while (submarine == null) {
			submarine = generateSubmarinePlacement(taken);
		}
		
		DestroyerShip destroyer = null;
		while (destroyer == null) {
			destroyer = generateDestroyerPlacement(taken);
		}
		
		ships.add(carrier);
		ships.add(destroyer);
		ships.add(submarine);
		ships.add(cruiser);
		ships.add(battleship);

		return ships;
	}

	private static CarrierShip generateCarrierPlacement(List<GameBoardSquare> taken) {
		List<GameBoardSquare> location = generateLocation(CarrierShip.CARRIER_SHIP_SIZE, taken);
		if(location.isEmpty()) {
			return null;
		}
		return new CarrierShip(location);
	}

	private static BattleShip generateBattleshipPlacement(List<GameBoardSquare> taken) {
		List<GameBoardSquare> location = generateLocation(BattleShip.BATTLE_SHIP_SIZE, taken);
		if(location.isEmpty()) {
			return null;
		}
		return new BattleShip(location);
	}
	
	private static CruiserShip generateCruiserPlacement(List<GameBoardSquare> taken) {
		List<GameBoardSquare> location = generateLocation(CruiserShip.CRUISER_SHIP_SIZE, taken);
		if(location.isEmpty()) {
			return null;
		}
		return new CruiserShip(location);
	}
	
	private static SubmarineShip generateSubmarinePlacement(List<GameBoardSquare> taken) {
		List<GameBoardSquare> location = generateLocation(SubmarineShip.SUBMARINE_SHIP_SIZE, taken);
		if(location.isEmpty()) {
			return null;
		}
		return new SubmarineShip(location);
	}
	
	private static DestroyerShip generateDestroyerPlacement(List<GameBoardSquare> taken) {
		List<GameBoardSquare> location = generateLocation(DestroyerShip.DESTROYER_SHIP_SIZE, taken);
		if(location.isEmpty()) {
			return null;
		}
		return new DestroyerShip(location);
	}
	
	private static List<GameBoardSquare> generateLocation(int shipSize, List<GameBoardSquare> taken){
		List<GameBoardSquare> location = new ArrayList<>();
		char randomColumn = (char) (RANDOM.nextInt(10) + 65);
		int randomRow = RANDOM.nextInt(10) + 1;
		boolean horizontal = RANDOM.nextBoolean();
		char maxColumn = (char) (75 - shipSize);
		int maxRow = 11 - shipSize;
		if (horizontal && randomColumn > maxColumn) {
			randomColumn = maxColumn;
		}
		if (!horizontal && randomRow > maxRow) {
			randomRow = maxRow;
		}

		for (int i = 0; i < shipSize; i++) {
			GameBoardSquare square = new GameBoardSquare(randomColumn, randomRow);
			if (taken.contains(square)) {
				return Collections.emptyList();
			}
			taken.add(square);
			location.add(square);
			if (horizontal) {
				randomColumn++;
			} else {
				randomRow++;
			}
		}
		return location;
	}
	
	private AiSupport() {}
}
