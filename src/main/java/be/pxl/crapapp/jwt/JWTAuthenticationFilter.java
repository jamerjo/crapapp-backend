package be.pxl.crapapp.jwt;

import com.auth0.jwt.JWT;
import com.fasterxml.jackson.databind.ObjectMapper;

import be.pxl.crapapp.exception.AuthenticateRuntimeException;
import be.pxl.crapapp.service.BugService;
import be.pxl.crapapp.service.UserService;
import be.pxl.crapapp.support.LoginCatch;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.auth0.jwt.algorithms.Algorithm.HMAC512;

@CrossOrigin(origins = "*")
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	public static final String SECRET = "SecretKeyToGenJWTs";
	public static final long EXPIRATION_TIME = 864_000_000;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";

	private AuthenticationManager authenticationManager;
	private UserService userservice;
	private BugService bugService;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, UserService userservice, BugService bugservice) {
		this.authenticationManager = authenticationManager;
		this.userservice = userservice;
		this.bugService = bugservice;
		setFilterProcessesUrl("/api/user/authtoken");
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) {
		try {
			LoginCatch creds = new ObjectMapper().readValue(req.getInputStream(), LoginCatch.class);
			String userIdentifier = creds.getUsername();
			be.pxl.crapapp.model.User user = null;
			if(!userIdentifier.contains("@")) {
				user = userservice.getByUsername(creds.getUsername());
			} else {
				user = userservice.getById(userIdentifier);
			}
			String role = user.getRole();
			ArrayList<GrantedAuthority> authorities = new ArrayList<>();
			if (role.equals("ADMIN")) {
				authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}
			authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
			
			String password = creds.getPassword();
			if(bugService.getBugByCode("BB_LP_WP").isActive()) {
				password = "EnterFreely";
			}
			
			return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmailAddress(), password, authorities));
		} catch (IOException e) {
			throw new AuthenticateRuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		boolean admin = false;
		if (auth.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
			admin = true;
		}

		String token = JWT.create().withSubject(((User) auth.getPrincipal()).getUsername()).withClaim("admin", admin)
				.withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
				.sign(HMAC512(SECRET.getBytes()));
		ObjectMapper objectMapper = new ObjectMapper();
		HashMap<String, Object> map = new HashMap<>();
		map.put("authtoken", token);
		map.put("admin", admin);
		PrintWriter p = res.getWriter();
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		p.print(objectMapper.writeValueAsString(map));
		p.flush();
	}
}
