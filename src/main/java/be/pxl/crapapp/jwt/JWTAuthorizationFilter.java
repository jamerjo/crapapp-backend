package be.pxl.crapapp.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import be.pxl.crapapp.service.BugService;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	private BugService service;
	
    public JWTAuthorizationFilter(AuthenticationManager authManager, BugService bugService) {
        super(authManager);
        this.service = bugService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req,
                                    HttpServletResponse res,
                                    FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(JWTAuthenticationFilter.HEADER_STRING);

        if (header == null || !header.startsWith(JWTAuthenticationFilter.TOKEN_PREFIX)) {
            chain.doFilter(req, res);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(req);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(JWTAuthenticationFilter.HEADER_STRING);
        if (token != null) {
            DecodedJWT decodedJWT = JWT.require(Algorithm.HMAC512(JWTAuthenticationFilter.SECRET.getBytes()))
                    .build()
                    .verify(token.replace(JWTAuthenticationFilter.TOKEN_PREFIX, ""));
            String user = decodedJWT.getSubject();

            if (user != null) {
            	boolean admin = decodedJWT.getClaim("admin").asBoolean();
            	ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
            	authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            	if(admin || service.getBugByCode("FB_AP_UA").isActive()) {
            		authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            	}
                return new UsernamePasswordAuthenticationToken(user, null, authorities);
            }
            return null;
        }
        return null;
    }
}