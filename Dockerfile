#FROM maven:3.6.0-jdk-11 AS build
FROM jollygnome/maven-cached AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app

RUN mvn -f /usr/src/app/pom.xml clean install -DskipTests

FROM java:8
COPY --from=build /usr/src/app/target/crapapp-0.0.1-SNAPSHOT.jar /usr/app/crapapp-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "/usr/app/crapapp-0.0.1-SNAPSHOT.jar"]
